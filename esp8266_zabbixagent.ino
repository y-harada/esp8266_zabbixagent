#include <Wire.h>
#include <WiFiClient.h>
#include <WiFiServer.h>
#include <WiFiUdp.h>

#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <EEPROM.h>
#include <ESP8266WebServer.h>
#include <Ticker.h>

//#define ZABBIXSERVER "zabbix.erscl.org"
IPAddress ZABBIXSERVER(192,168,35,14);
#define ZABBIXPORT 10051
#define ZABBIXAGHOST "skoll"
#define ZABBIXMAXLEN 256

uint8_t zabbixPacket[ZABBIXMAXLEN];
int zabbixPacketLen;

// Pin definitions
#define BLUE_LED 14
#define RED_LED 12

boolean restoreConfig();
boolean checkConnection();
void startWebServer();
void setupMode();
String makePage(String title, String contents);
String urlDecode(String input);
void handleCommand();
void handleSht21();
float Sht21_temp();
float Sht21_hum();

void timerFunction();

enum  led_style {led_off = 0, led_on = 1, slow_blink = 2, fast_blink = 3 } ;
static enum led_style blue_led = led_off;

const IPAddress apIP(192, 168, 1, 1);
const char* apSSID = "ESP8266_SETUP";
boolean settingMode;
String ssidList;

DNSServer dnsServer;
ESP8266WebServer webServer(80);

WiFiClient zClient;

// SHT-21
#define SHT21_ADDR 0x40
#define CMD_RESET 0xFE
#define CMD_T 0xF3
#define CMD_RH 0xF5
float t_dat;
float rh_dat;

void setup() {
  Wire.begin();
  Serial.begin(115200);
  EEPROM.begin(512);
  delay(10);

 // WiFi AP OFF
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  
  //LED init
  pinMode(BLUE_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);

  digitalWrite(BLUE_LED, 0);
  digitalWrite(RED_LED, 0);
  if (restoreConfig()) {
    if (checkConnection()) {
      settingMode = false;
      startWebServer();
      return;
    }
  }
  settingMode = true;
  setupMode();
}

void loop() {
  if (settingMode) {
    dnsServer.processNextRequest();
  }
  
  webServer.handleClient();
  
  static uint32_t counter = 0;
  if( (counter++)%1000 == 0){
  	timerFunction();
  }
  delay(1);
}

boolean restoreConfig() {
  Serial.println("Reading EEPROM...");
  String ssid = "";
  String pass = "";
  if (EEPROM.read(0) != 0) {
    for (int i = 0; i < 32; ++i) {
      ssid += char(EEPROM.read(i));
    }
    Serial.print("SSID: ");
    Serial.println(ssid);
    for (int i = 32; i < 96; ++i) {
      pass += char(EEPROM.read(i));
    }
    Serial.print("Password: ");
    Serial.println(pass);
    WiFi.begin(ssid.c_str(), pass.c_str());
    return true;
  }
  else {
    Serial.println("Config not found.");
    return false;
  }
}

boolean checkConnection() {
  int count = 0;
  Serial.print("Waiting for Wi-Fi connection");
  while ( count < 300 ) {
    if (WiFi.status() == WL_CONNECTED) {
      Serial.println();
      Serial.println("Connected!");
      return (true);
    }
    delay(500);
    Serial.print(".");
    count++;
  }
  Serial.println("Timed out.");
  return false;
}

void startWebServer() {
  if (settingMode) {
    Serial.print("Starting Web Server at ");
    Serial.println(WiFi.softAPIP());
		// display_ipaddr(WiFi.softAPIP());
    webServer.on("/settings", []() {
      String s = "<h1>Wi-Fi Settings</h1><p>Please enter your password by selecting the SSID.</p>";
      s += "<form method=\"get\" action=\"setap\"><label>SSID: </label><select name=\"ssid\">";
      s += ssidList;
      s += "</select><br>Password: <input name=\"pass\" length=64 type=\"password\"><input type=\"submit\"></form>";
      webServer.send(200, "text/html", makePage("Wi-Fi Settings", s));
    });
    webServer.on("/setap", []() {
      for (int i = 0; i < 96; ++i) {
        EEPROM.write(i, 0);
      }
      String ssid = urlDecode(webServer.arg("ssid"));
      Serial.print("SSID: ");
      Serial.println(ssid);
      String pass = urlDecode(webServer.arg("pass"));
      Serial.print("Password: ");
      Serial.println(pass);
      Serial.println("Writing SSID to EEPROM...");
      for (int i = 0; i < ssid.length(); ++i) {
        EEPROM.write(i, ssid[i]);
      }
      for (int i = 0; i < pass.length(); ++i) {
        EEPROM.write(32 + i, pass[i]);
      }
      EEPROM.commit();
      Serial.println("Write EEPROM done!");
      String s = "<h1>Setup complete.</h1><p>device will be connected to \"";
      s += ssid;
      s += "\" after the restart.";
      webServer.send(200, "text/html", makePage("Wi-Fi Settings", s));
      ESP.restart();
    });
    webServer.onNotFound([]() {
      String s = "<h1>AP mode</h1><p><a href=\"/settings\">Wi-Fi Settings</a></p>";
      webServer.send(200, "text/html", makePage("AP mode", s));
    });
  }
  else {
    Serial.print("Starting Web Server at ");
    Serial.println(WiFi.localIP());
    // display_ipaddr(WiFi.localIP());
    webServer.on("/", []() {
      String s = "<h1>STA mode</h1><p><a href=\"/reset\">Reset Wi-Fi Settings</a></p>";
      webServer.send(200, "text/html", makePage("STA mode", s));
    });
    webServer.on("/sht21", handleSht21);
    webServer.on("/cmd", handleCommand);
    webServer.on("/reset", []() {
      for (int i = 0; i < 96; ++i) {
        EEPROM.write(i, 0);
      }
      EEPROM.commit();
      String s = "<h1>Wi-Fi settings was reset.</h1><p>Please reset device.</p>";
      webServer.send(200, "text/html", makePage("Reset Wi-Fi Settings", s));
    });
  }
  webServer.begin();
}

void setupMode() {
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  int n = WiFi.scanNetworks();
  delay(100);
  Serial.println("");
  for (int i = 0; i < n; ++i) {
    ssidList += "<option value=\"";
    ssidList += WiFi.SSID(i);
    ssidList += "\">";
    ssidList += WiFi.SSID(i);
    ssidList += "</option>";
  }
  delay(100);
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP(apSSID);
  dnsServer.start(53, "*", apIP);
  startWebServer();
  Serial.print("Starting Access Point at \"");
  Serial.print(apSSID);
  Serial.println("\"");
}

String makePage(String title, String contents) {
  String s = "<!DOCTYPE html><html><head>";
  s += "<meta name=\"viewport\" content=\"width=device-width,user-scalable=0\">";
  s += "<title>";
  s += title;
  s += "</title></head><body>";
  s += contents;
  s += "</body></html>";
  return s;
}

String urlDecode(String input) {
  String s = input;
  s.replace("%20", " "); s.replace("+", " "); s.replace("%21", "!"); s.replace("%22", "\"");  s.replace("%23", "#"); s.replace("%24", "$");  s.replace("%25", "%");  s.replace("%26", "&");  s.replace("%27", "\'");  s.replace("%28", "(");
  s.replace("%29", ")");  s.replace("%30", "*");  s.replace("%31", "+");  s.replace("%2C", ",");  s.replace("%2E", ".");  s.replace("%2F", "/");  s.replace("%2C", ",");  s.replace("%3A", ":");
  s.replace("%3A", ";");  s.replace("%3C", "<");  s.replace("%3D", "=");  s.replace("%3E", ">");  s.replace("%3F", "?");  s.replace("%40", "@");  s.replace("%5B", "[");  s.replace("%5C", "\\");
  s.replace("%5D", "]");  s.replace("%5E", "^");  s.replace("%5F", "-");  s.replace("%60", "`");
  return s;
}

void send_bad_request() {
  webServer.send(400, "text/html", "error");
  Serial.println("Bad request");
}

float Sht21_reset(){
  // SOFT-RESET
  Wire.beginTransmission(SHT21_ADDR);
  Wire.write(CMD_RESET);
  Wire.endTransmission();
  delay(500);
}

uint16_t Sht21_readdat(uint8_t CMD){
	uint8_t msb=0, lsb=0, chk=0; // 読み出す3byte
  // コマンドの送信
  // 温度計測
  Wire.beginTransmission(SHT21_ADDR);
  Wire.write(CMD);
  Wire.endTransmission();
  delay(100);

  // 温度読み取り
  Wire.requestFrom(SHT21_ADDR,3);
  msb = Wire.read();
  lsb = Wire.read();
  chk = Wire.read();
  uint16_t dat = msb << 8 | lsb;
  return dat;
}

float Sht21_temp(){
	uint8_t msb=0, lsb=0, chk=0; // 読み出す3byte
  t_dat = -46.85 + 175.72 * (float)Sht21_readdat(CMD_T)/ (16.0 * 16.0 * 16.0 * 16.0);
  Serial.print("T = ");
  Serial.println(t_dat);
  return t_dat;
}

float Sht21_hum(){
	uint8_t msb=0, lsb=0, chk=0; // 読み出す3byte
  float rh_dat = -6.0 + 125.0 * (float)Sht21_readdat(CMD_RH)  / (16.0 * 16.0 * 16.0 * 16.0);
  Serial.print("RH = ");
  Serial.println(rh_dat);
  return rh_dat;
	
}

void handleSht21(){
  digitalWrite(BLUE_LED, 1);
	char s[16];
	float t = Sht21_temp();
	float rh = Sht21_hum();
	String response = "{\"temperature\": " + String(dtostrf(t, 5, 2, s)) + ", \"humidity\": " + String(dtostrf(rh, 5, 2, s)) + "}";
	Serial.println(response);
	webServer.send(200, "text/html", response);
  digitalWrite(BLUE_LED, 0);
}

void handleCommand() {
  if (webServer.hasArg("LED")) {
    String cmd = webServer.arg("LED");
    Serial.println("handleCommand() LED=" + cmd);
    if (cmd == "on")  {
      digitalWrite(BLUE_LED, 1);
    }
    else if (cmd == "off") {
      digitalWrite(BLUE_LED, 0);
    }
  }  else {
    send_bad_request();
    return;
  }
	webServer.send(200, "text/html", "OK\r\n");
}

int create_zabbixPacket(String Json){
	for(int i=0;i<ZABBIXMAXLEN;i++){
		zabbixPacket[i] = 0;		
	}
	zabbixPacket[0] = 'Z';
	zabbixPacket[1] = 'B';
	zabbixPacket[2] = 'X';
	zabbixPacket[3] = 'D';
	zabbixPacket[4] = 0x01;
	uint64_t JsonLen = Json.length();
	uint64_t remLen = JsonLen;
	for(int i=0;i<8;i++){
		zabbixPacket[5+i] = (remLen % 256);
		remLen = (uint64_t) remLen /  256;
	}
	Json.getBytes(&(zabbixPacket[13]), ZABBIXMAXLEN-12);
	Serial.print("request = ");
	for(int i=0;i<ZABBIXMAXLEN;i++){
		Serial.print((char)(zabbixPacket[i]));		
	}
	Serial.println();
	return 13+JsonLen;
}

String create_zabbixJson(float temp, float hum){
	char s[16];
	String Json = "{\"request\":\"sender data\",\"data\":[{\"host\":\"" + String(ZABBIXAGHOST) + "\",\"key\":\"air.temp\",\"value\":\"" + String(dtostrf(temp, 6, 3, s)) + "\"},{\"host\":\"" + String(ZABBIXAGHOST) + "\",\"key\":\"air.hum\",\"value\":\"" + String(dtostrf(hum, 6, 3, s)) + "\"}]}";
	return Json;
}

void timerFunction(){
  static int timer = 0;
	// Serial.println(String(timer));
	if (  WiFi.status() != WL_CONNECTED ){
  	digitalWrite(RED_LED, 1);
		Serial.println("WiFI: NG");
		if ( ! checkConnection() ){
			return;	
		}else {
			 digitalWrite(RED_LED, 0);
		}
	}
	if (timer == 0){
		digitalWrite(BLUE_LED, 1);
		float t = Sht21_temp();
		float rh = Sht21_hum();
		int packetLen = create_zabbixPacket(create_zabbixJson(t, rh));
		if ( zClient.connect(ZABBIXSERVER, ZABBIXPORT)){
			zClient.write(&zabbixPacket[0], packetLen);
			delay(200);
			if (zClient.available()){
				Serial.print("result = ");
				while (zClient.available()){
					Serial.print(zClient.readString());				
				}
				Serial.println();
				Serial.println("Send Data");				
  			digitalWrite(RED_LED, 0);
			} else{
				Serial.println("No result");
  			digitalWrite(RED_LED, 1);
			}
			delay(10);
			zClient.stop();
		}
		digitalWrite(BLUE_LED, 0);
	}
  timer= (timer+1)%40;
}

